import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
	selector: 'app-banner',
	templateUrl: './banner.component.html',
	styleUrls: [ './banner.component.scss' ]
})
export class BannerComponent implements OnInit {
	customOptions: OwlOptions = {
		loop: true,
		autoplay: true,
		mouseDrag: true,
		touchDrag: true,
		pullDrag: false,
		dots: true,
		navSpeed: 1000,
		navText: [ '', '' ],
		responsive: {
			0: {
				items: 1
			},
			400: {
				items: 1
			},
			740: {
				items: 1
			},
			940: {
				items: 1
			}
		},
		nav: true
	};

	constructor() {}

	ngOnInit() {}
}

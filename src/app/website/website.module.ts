import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { WebsiteRoutingModule } from './website-routing.module';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './includes/header/header.component';
import { FooterComponent } from './includes/footer/footer.component';
import { MaterialModule } from './../material/material.module';
import { BannerComponent } from './home/banner/banner.component';
import { BlogComponent } from './home/blog/blog.component';

@NgModule({
	declarations: [ HomeComponent, HeaderComponent, FooterComponent, MainComponent, BannerComponent, BlogComponent ],
	imports: [ CommonModule, WebsiteRoutingModule, FlexLayoutModule, MaterialModule, MatCarouselModule, CarouselModule ]
})
export class WebsiteModule {}
